From: uazo <uazo@users.noreply.github.com>
Date: Sat, 13 Nov 2021 09:17:06 +0000
Subject: Disable FLoC and privacy sandbox

Remove UI from the settings and set the flags to inactive
Permanently removes FLoC support, disabling the download of LSH clusters,
the marking the history navigation and the javascript API and permission policies.

Original License: GPL-2.0-or-later - https://spdx.org/licenses/GPL-2.0-or-later.html
License: GPL-3.0-only - https://spdx.org/licenses/GPL-3.0-only.html
---
 .../browser/chrome_content_browser_client.cc  |  3 +++
 .../privacy_sandbox_settings_delegate.cc      |  1 +
 .../privacy_sandbox/privacy_sandbox_prompt.cc |  1 +
 .../history/core/browser/history_backend.cc   | 13 +---------
 .../privacy_sandbox_attestations.cc           |  2 --
 .../privacy_sandbox_features.cc               |  6 ++++-
 .../privacy_sandbox/privacy_sandbox_prefs.cc  |  4 ++--
 .../privacy_sandbox_settings_impl.cc          | 24 +++++++++++++------
 8 files changed, 30 insertions(+), 24 deletions(-)

diff --git a/chrome/browser/chrome_content_browser_client.cc b/chrome/browser/chrome_content_browser_client.cc
--- a/chrome/browser/chrome_content_browser_client.cc
+++ b/chrome/browser/chrome_content_browser_client.cc
@@ -3381,6 +3381,9 @@ bool ChromeContentBrowserClient::IsAttributionReportingOperationAllowed(
     const url::Origin* destination_origin,
     const url::Origin* reporting_origin,
     bool* can_bypass) {
+  // make sure that this is always disabled in Bromite even if privacy sandbox is already disabled in the prefs
+  if ((true))
+    return false;
   Profile* profile = Profile::FromBrowserContext(browser_context);
 
   auto* privacy_sandbox_settings =
diff --git a/chrome/browser/privacy_sandbox/privacy_sandbox_settings_delegate.cc b/chrome/browser/privacy_sandbox/privacy_sandbox_settings_delegate.cc
--- a/chrome/browser/privacy_sandbox/privacy_sandbox_settings_delegate.cc
+++ b/chrome/browser/privacy_sandbox/privacy_sandbox_settings_delegate.cc
@@ -159,6 +159,7 @@ bool PrivacySandboxSettingsDelegate::HasAppropriateTopicsConsent() const {
 
 bool PrivacySandboxSettingsDelegate::PrivacySandboxRestrictedNoticeRequired()
     const {
+  if ((true)) return true;
   auto* identity_manager = IdentityManagerFactory::GetForProfile(profile_);
 
   if (!identity_manager ||
diff --git a/chrome/browser/ui/privacy_sandbox/privacy_sandbox_prompt.cc b/chrome/browser/ui/privacy_sandbox/privacy_sandbox_prompt.cc
--- a/chrome/browser/ui/privacy_sandbox/privacy_sandbox_prompt.cc
+++ b/chrome/browser/ui/privacy_sandbox/privacy_sandbox_prompt.cc
@@ -8,5 +8,6 @@
 
 void ShowPrivacySandboxPrompt(Browser* browser,
                               PrivacySandboxService::PromptType prompt_type) {
+    if ((true)) return;
     ShowPrivacySandboxDialog(browser, prompt_type);
 }
diff --git a/components/history/core/browser/history_backend.cc b/components/history/core/browser/history_backend.cc
--- a/components/history/core/browser/history_backend.cc
+++ b/components/history/core/browser/history_backend.cc
@@ -655,18 +655,7 @@ void HistoryBackend::SetBrowsingTopicsAllowed(ContextID context_id,
   if (!visit_id)
     return;
 
-  // Only add to the annotations table if the visit_id exists in the visits
-  // table.
-  VisitContentAnnotations annotations;
-  if (db_->GetContentAnnotationsForVisit(visit_id, &annotations)) {
-    annotations.annotation_flags |=
-        VisitContentAnnotationFlag::kBrowsingTopicsEligible;
-    db_->UpdateContentAnnotationsForVisit(visit_id, annotations);
-  } else {
-    annotations.annotation_flags |=
-        VisitContentAnnotationFlag::kBrowsingTopicsEligible;
-    db_->AddContentAnnotationsForVisit(visit_id, annotations);
-  }
+  // in Bromite disallow marking anything in history related to topics
   ScheduleCommit();
 }
 
diff --git a/components/privacy_sandbox/privacy_sandbox_attestations/privacy_sandbox_attestations.cc b/components/privacy_sandbox/privacy_sandbox_attestations/privacy_sandbox_attestations.cc
--- a/components/privacy_sandbox/privacy_sandbox_attestations/privacy_sandbox_attestations.cc
+++ b/components/privacy_sandbox/privacy_sandbox_attestations/privacy_sandbox_attestations.cc
@@ -405,8 +405,6 @@ void PrivacySandboxAttestations::OnAttestationsParsed(
   if (attestations_map.has_value() &&
       (!file_version_.IsValid() || file_version_.CompareTo(version) < 0)) {
     // Parsing succeeded and the attestations file has newer version.
-    file_version_ = std::move(version);
-    attestations_map_ = std::move(attestations_map.value());
   }
 
   attestations_parse_progress_ = Progress::kFinished;
diff --git a/components/privacy_sandbox/privacy_sandbox_features.cc b/components/privacy_sandbox/privacy_sandbox_features.cc
--- a/components/privacy_sandbox/privacy_sandbox_features.cc
+++ b/components/privacy_sandbox/privacy_sandbox_features.cc
@@ -145,11 +145,15 @@ BASE_FEATURE(kAttributionDebugReportingCookieDeprecationTesting,
 BASE_FEATURE(kPrivateAggregationDebugReportingCookieDeprecationTesting,
              "PrivateAggregationDebugReportingCookieDeprecationTesting",
              base::FEATURE_DISABLED_BY_DEFAULT);
-             
+
 #if BUILDFLAG(IS_ANDROID)
 BASE_FEATURE(kTrackingProtectionNoticeRequestTracking,
              "TrackingProtectionNoticeRequestTracking",
              base::FEATURE_DISABLED_BY_DEFAULT);
 #endif  // BUILDFLAG(IS_ANDROID)
 
+SET_CROMITE_FEATURE_DISABLED(kPrivacySandboxSettings4);
+SET_CROMITE_FEATURE_ENABLED(kDisablePrivacySandboxPrompts);
+SET_CROMITE_FEATURE_DISABLED(kEnforcePrivacySandboxAttestations);
+SET_CROMITE_FEATURE_DISABLED(kPrivacySandboxFirstPartySetsUI);
 }  // namespace privacy_sandbox
diff --git a/components/privacy_sandbox/privacy_sandbox_prefs.cc b/components/privacy_sandbox/privacy_sandbox_prefs.cc
--- a/components/privacy_sandbox/privacy_sandbox_prefs.cc
+++ b/components/privacy_sandbox/privacy_sandbox_prefs.cc
@@ -12,7 +12,7 @@ namespace privacy_sandbox {
 
 void RegisterProfilePrefs(PrefRegistrySimple* registry) {
   registry->RegisterBooleanPref(
-      prefs::kPrivacySandboxApisEnabled, true,
+      prefs::kPrivacySandboxApisEnabled, false,
       user_prefs::PrefRegistrySyncable::SYNCABLE_PREF);
   registry->RegisterBooleanPref(prefs::kPrivacySandboxApisEnabledV2, false);
   registry->RegisterBooleanPref(prefs::kPrivacySandboxM1ConsentDecisionMade,
@@ -28,7 +28,7 @@ void RegisterProfilePrefs(PrefRegistrySimple* registry) {
   registry->RegisterBooleanPref(prefs::kPrivacySandboxM1FledgeEnabled, false);
   registry->RegisterBooleanPref(prefs::kPrivacySandboxM1AdMeasurementEnabled,
                                 false);
-  registry->RegisterBooleanPref(prefs::kPrivacySandboxM1Restricted, false);
+  registry->RegisterBooleanPref(prefs::kPrivacySandboxM1Restricted, true);
 
   registry->RegisterBooleanPref(prefs::kPrivacySandboxManuallyControlledV2,
                                 false);
diff --git a/components/privacy_sandbox/privacy_sandbox_settings_impl.cc b/components/privacy_sandbox/privacy_sandbox_settings_impl.cc
--- a/components/privacy_sandbox/privacy_sandbox_settings_impl.cc
+++ b/components/privacy_sandbox/privacy_sandbox_settings_impl.cc
@@ -121,7 +121,7 @@ std::set<browsing_topics::Topic> GetTopicsSetFromString(
 
 // static
 bool PrivacySandboxSettingsImpl::IsAllowed(Status status) {
-  return status == Status::kAllowed;
+  return false;
 }
 
 // static
@@ -230,7 +230,8 @@ PrivacySandboxSettingsImpl::GetFinchPrioritizedTopics() {
   return finch_prioritized_topics_;
 }
 
-bool PrivacySandboxSettingsImpl::IsTopicsAllowed() const {
+bool PrivacySandboxSettingsImpl::IsTopicsAllowed() const { // disabled in Bromite
+  if ((true)) return false;
   Status status = GetM1TopicAllowedStatus();
   JoinHistogram(kIsTopicsAllowedHistogram, status);
   return IsAllowed(status);
@@ -262,7 +263,8 @@ bool PrivacySandboxSettingsImpl::IsTopicsAllowedForContext(
   return IsAllowed(status);
 }
 
-bool PrivacySandboxSettingsImpl::IsTopicAllowed(const CanonicalTopic& topic) {
+bool PrivacySandboxSettingsImpl::IsTopicAllowed(const CanonicalTopic& topic) { // disabled in Bromite
+  if ((true)) return false;
   const auto& blocked_topics =
       pref_service_->GetList(prefs::kPrivacySandboxBlockedTopics);
 
@@ -464,6 +466,7 @@ bool PrivacySandboxSettingsImpl::
 void PrivacySandboxSettingsImpl::SetFledgeJoiningAllowed(
     const std::string& top_frame_etld_plus1,
     bool allowed) {
+  if ((true)) return;
   ScopedDictPrefUpdate scoped_pref_update(
       pref_service_, prefs::kPrivacySandboxFledgeJoinBlocked);
 
@@ -534,7 +537,8 @@ void PrivacySandboxSettingsImpl::ClearFledgeJoiningAllowedSettings(
 }
 
 bool PrivacySandboxSettingsImpl::IsFledgeJoiningAllowed(
-    const url::Origin& top_frame_origin) const {
+    const url::Origin& top_frame_origin) const { // disabled in Bromite
+  if ((true)) return false;
   ScopedDictPrefUpdate scoped_pref_update(
       pref_service_, prefs::kPrivacySandboxFledgeJoinBlocked);
   auto& pref_data = scoped_pref_update.Get();
@@ -693,7 +697,8 @@ bool PrivacySandboxSettingsImpl::IsPrivateAggregationDebugModeAllowed(
          delegate_->AreThirdPartyCookiesBlockedByCookieDeprecationExperiment();
 }
 
-bool PrivacySandboxSettingsImpl::IsPrivacySandboxEnabled() const {
+bool PrivacySandboxSettingsImpl::IsPrivacySandboxEnabled() const { // disabled in Bromite
+  if ((true)) return false;
   PrivacySandboxSettingsImpl::Status status = GetPrivacySandboxAllowedStatus();
   if (!IsAllowed(status)) {
     return false;
@@ -720,7 +725,8 @@ void PrivacySandboxSettingsImpl::SetTopicsBlockedForTesting() {
   pref_service_->SetBoolean(prefs::kPrivacySandboxM1TopicsEnabled, false);
 }
 
-void PrivacySandboxSettingsImpl::SetPrivacySandboxEnabled(bool enabled) {
+void PrivacySandboxSettingsImpl::SetPrivacySandboxEnabled(bool enabled) { // disabled in Bromite
+  enabled = false;
   pref_service_->SetBoolean(prefs::kPrivacySandboxApisEnabledV2, enabled);
 }
 
@@ -767,7 +773,10 @@ void PrivacySandboxSettingsImpl::SetDelegateForTesting(
   delegate_ = std::move(delegate);
 }
 
-void PrivacySandboxSettingsImpl::SetTopicsDataAccessibleFromNow() const {
+void PrivacySandboxSettingsImpl::SetTopicsDataAccessibleFromNow() const { // disabled in Bromite
+  pref_service_->ClearPref(prefs::kPrivacySandboxTopicsDataAccessibleSince);
+  if ((true)) return;
+
   pref_service_->SetTime(prefs::kPrivacySandboxTopicsDataAccessibleSince,
                          base::Time::Now());
 
@@ -780,6 +789,7 @@ PrivacySandboxSettingsImpl::Status
 PrivacySandboxSettingsImpl::GetSiteAccessAllowedStatus(
     const url::Origin& top_frame_origin,
     const GURL& url) const {
+  if ((true)) return Status::kSiteDataAccessBlocked;
   // Relying on |host_content_settings_map_| instead of |cookie_settings_|
   // allows to query whether the site associated with the |url| is allowed to
   // access Site data (aka ContentSettingsType::COOKIES) without considering any
--
